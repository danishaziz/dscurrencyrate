//
//  CurrencyRateViewModel.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal class CurrencyRateViewModel {
    
    let model: CurrencyRateModel = CurrencyRateModel()
    
    func loadCurrencyRate(completion: @escaping (CurrencyRate?) -> Void) {
        if let url = URL(string: Constant.currencyRateUrl) {
            model.loadCurrencyRate(from: url) { (currencyRate, error) in
                
                if error != nil {
                    completion(nil)
                }
                
                completion(currencyRate)
            }
        } else {
            completion(nil)
        }
    }
}

