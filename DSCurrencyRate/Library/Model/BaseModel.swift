//
//  BaseModel.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal class BaseModel {

    var api: API = API()

    func cancelDownload(from url: URL) {
        api.cancelDownload(from: url)
    }
}
