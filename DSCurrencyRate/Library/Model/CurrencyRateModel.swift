//
//  CurrencyRateModel.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal class CurrencyRateModel: BaseModel {
    
    func loadCurrencyRate(from url: URL, completion: @escaping (CurrencyRate? , Error?) -> Void) {
        api.getData(from: url, dataType: .json) { (data, error) in
            
            if error != nil {
                completion(nil, error)
            }
            
            if let jsonData = data {
                let currency = try? JSONDecoder().decode(Currency.self, from: jsonData)
                let currencyRate = currency.map( { $0.data.brands.wbc.portfolios.fx.currencyRate })
                completion(currencyRate, nil)
            }
        }
    }
}
