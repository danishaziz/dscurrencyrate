//
//  DataType.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal enum DataType {
    case json
    case xml
}
