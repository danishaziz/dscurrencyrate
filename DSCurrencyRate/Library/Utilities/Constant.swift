//
//  Constant.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal class Constant {

    static let currencyRateUrl: String = "https://www.westpac.com.au/bin/getJsonRates.wbc.fx.json"
}
