//
//  API.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

internal class API {
    
    var downloader: Downloader = Downloader()
    
    func getData(from url: URL, dataType: DataType, completion: @escaping (Data?, Error?) -> Void) {
        
        downloader.getData(from: url) { (data, response, error) in
            if dataType == DataType.json {
                if let data = data {
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            completion(data, error)
                        }
                    }
                }
            }
            completion(nil, nil)
        }
    }
    
    func cancelDownload(from url: URL) {
        downloader.cancelDownload(url: url)
    }
}

