//
//  DSCurrencyRate.swift
//  DSCurrencyRate
//
//  Created by Danish Aziz on 21/11/19.
//  Copyright © 2019 Danish Aziz. All rights reserved.
//

import Foundation

public class DSCurrencyRate {

    private static var sharedInstance: DSCurrencyRate!
    fileprivate let viewModel: CurrencyRateViewModel = CurrencyRateViewModel()
    private init() {
    }

    public static func shared() -> DSCurrencyRate {
        if sharedInstance != nil {
            return sharedInstance
        } else{
            sharedInstance = DSCurrencyRate()
            return sharedInstance
        }
    }

    public func loadCurrencyRate(completion: @escaping (CurrencyRate?) -> Void) {
        viewModel.loadCurrencyRate() { currencyRate in
            completion(currencyRate)
        }
    }
}
