#
#  Be sure to run `pod spec lint DSCurrencyRate.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'

s.name = "DSCurrencyRate"
s.summary = "Currency Rate Exchange Library."
s.requires_arc = true

# 2
s.version = "1.0.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Danish Aziz" => "danishaziz1989@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/danishaziz"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/danishaziz/dscurrencyrate.git", 
             :tag => "#{s.version}" }

# 7
s.framework = "Foundation"

# 8
s.source_files = "dscurrencyrate/**/*.{swift}"

# 9
s.resources = "dscurrencyrate/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.0"

end
